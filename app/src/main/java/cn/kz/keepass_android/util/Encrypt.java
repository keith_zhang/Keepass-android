package cn.kz.keepass_android.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * Created by kz on 2017/3/6.
 */

public class Encrypt {

    public static String encrypt(String msg, String key, String type) throws NoSuchAlgorithmException {
        KeyGenerator instance = KeyGenerator.getInstance(type);
        instance.init(new SecureRandom(key.getBytes()));
        SecretKey kgen = instance.generateKey();

        String encrypt = null;
        try {
            encrypt = CipherUtils.encrypt(msg, kgen, type);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return encrypt;
    }


    public static String decrypt(String encrypt, String key, String type) throws NoSuchAlgorithmException {
        KeyGenerator instance = KeyGenerator.getInstance(type);
        instance.init(new SecureRandom(key.getBytes()));
        SecretKey kgen = instance.generateKey();
        String decrypt = null;
        try {
            decrypt = CipherUtils.decrypt(encrypt, kgen, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decrypt;
    }

}
