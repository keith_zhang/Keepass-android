package cn.kz.keepass_android.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kz on 2017/3/8.
 */
public class SPUtil {


    public static String getString(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.KEEPASSDB, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, null);
    }

    public static void setString(Context context, String key, String value) {
        SharedPreferences sp = context.getSharedPreferences(Constant.KEEPASSDB, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.apply();
    }
}
