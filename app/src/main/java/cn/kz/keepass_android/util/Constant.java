package cn.kz.keepass_android.util;

/**
 * Created by kz on 2017/3/8.
 */

public class Constant {

    public static final String KEEPASSDB = "keepassDB";
    public static final String SERVER_ADDR = "server";
    public static final String DB_PATH = "dbpath";
    public static final String PASSWORD = "password";


    public static final String QRCODE_RESULT = "qrcode";


    public static final String KEY = "key";
    public static final String TYPE = "type";
    public static final String HREF = "href";
    public static final String ID = "id";


}
