package cn.kz.keepass_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by kz on 2017/3/4.
 */

public class DetailActivity extends AppCompatActivity {
    private TextView title, username, password;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail);


        title = (TextView) findViewById(R.id.title);
        username = (TextView) findViewById(R.id.username);
        password = (TextView) findViewById(R.id.password);

        Intent intent = getIntent();
        title.setText(intent.getStringExtra("title"));
        username.setText(intent.getStringExtra("username"));
        password.setText(intent.getStringExtra("password"));

    }


}
